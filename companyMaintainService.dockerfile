FROM openjdk:11
COPY ./maintain-company-data/ /maintain-company-data/
WORKDIR /maintain-company-data
RUN curl -L https://services.gradle.org/distributions/gradle-6.9-bin.zip -o gradle-6.9-bin.zip
RUN apt update
RUN apt-get install -y unzip
RUN apt-get install -y curl
RUN unzip gradle-6.9-bin.zip
ENV GRADLE_HOME=/maintain-company-data/gradle-6.9
ENV PATH=$PATH:$GRADLE_HOME/bin
RUN gradle build
EXPOSE 8090
CMD ["gradle", "bootRun"]
