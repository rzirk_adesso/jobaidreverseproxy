FROM openjdk:11
COPY ./jobaidcompanydataservice/ /jobaidcompanydataservice/
WORKDIR /jobaidcompanydataservice
RUN curl -L https://services.gradle.org/distributions/gradle-6.9-bin.zip -o gradle-6.9-bin.zip
RUN apt update
RUN apt-get install -y unzip
RUN apt-get install -y curl
RUN unzip gradle-6.9-bin.zip
ENV GRADLE_HOME=/jobaidcompanydataservice/gradle-6.9
ENV PATH=$PATH:$GRADLE_HOME/bin
#RUN apt install bash vim curl wget jq docker git tar unzip bash-completion ca-certificates gradle maven -y
RUN gradle build
EXPOSE 8090
CMD ["gradle", "bootRun"]
